import 'package:flutter/material.dart';
import '../theme.dart';

class CustomPrimaryButton extends StatelessWidget {
  final Color? buttonColor;
  final String text;
  final Color? textColor;
  final VoidCallback onPressed;
  final bool isLoading;

  CustomPrimaryButton(
      {this.buttonColor,
      required this.text,
      this.textColor,
      this.isLoading = false,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: double.infinity,
      height: 65,
      onPressed: onPressed,
      elevation: 0,
      color: buttonColor ?? primaryBlue,
      textColor: textColor ?? Colors.white,
      child: isLoading
          ? CircularProgressIndicator(
              strokeWidth: 2,
            )
          : Text(text.toUpperCase()),
    );
  }
}
