import 'package:flutter/material.dart';
import '../theme.dart';
import 'widgets.dart';

class CustomImageField extends StatelessWidget {
  CustomImageField({
    Key? key,
    required this.onPressed,
    this.title,
  }) : super(key: key);

  final VoidCallback onPressed;
  String? title;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        height: 50,
        alignment: Alignment.center,
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3),
          border: Border.all(color: textGrey, width: 1),
          color: Colors.transparent,
        ),
        child: Text(
          title ?? 'Select images to upload',
          style: TextStyle(color: textGrey),
        ),
      ),
    );
  }
}
