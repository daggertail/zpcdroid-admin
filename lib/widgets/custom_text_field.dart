import 'package:flutter/material.dart';
import '../theme.dart';

class CustomTextField extends StatelessWidget {
  final String hintText;
  final String? Function(String?)? validator;
  final TextEditingController controller;
  final bool obscureText;
  final Widget? suffixIcon;
  final bool isNumeric;
  final bool isMultiline;
  final bool isReadOnly;
  Function(String)? onFieldSubmitted;

  CustomTextField({
    Key? key,
    required this.hintText,
    required this.controller,
    this.obscureText = false,
    this.validator,
    this.suffixIcon,
    this.onFieldSubmitted,
    this.isNumeric = false,
    this.isMultiline = false,
    this.isReadOnly = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextInputType keyboardType;
    if (isNumeric) {
      keyboardType = TextInputType.number;
    } else if (isMultiline) {
      keyboardType = TextInputType.multiline;
    } else {
      keyboardType = TextInputType.text;
    }

    return IntrinsicHeight(
      child: TextFormField(
        keyboardType: keyboardType,
        minLines: null,
        maxLines: obscureText ? 1 : null,
        expands: obscureText ? false : true,
        obscureText: obscureText,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: validator,
        controller: controller,
        enabled: !isReadOnly,
        onFieldSubmitted: onFieldSubmitted,
        decoration: InputDecoration(
            fillColor: Colors.transparent,
            hintText: hintText,
            hintStyle: heading6.copyWith(
                color: textGrey, fontWeight: FontWeight.normal),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: primaryBlue, width: 1),
            ),
            suffixIcon: suffixIcon),
      ),
    );
  }
}
