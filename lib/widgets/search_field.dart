import 'package:flutter/material.dart';
import '../../../constants.dart';

class SearchField extends StatelessWidget {
  SearchField({
    Key? key,
    required this.onChanged,
    required this.searchFieldController,
  }) : super(key: key);

  final Function(String)? onChanged;
  final TextEditingController searchFieldController;

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        hintText: "Search",
        fillColor: secondaryColor,
        filled: true,
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        suffixIcon: Container(
          margin: EdgeInsets.only(right: 10),
          child: Icon(Icons.search),
        ),
      ),
      controller: searchFieldController,
      onChanged: onChanged,
    );
  }
}
