import 'package:flutter/material.dart';

class CustomImageThumbnail extends StatelessWidget {
  CustomImageThumbnail({
    Key? key,
    required this.image,
    this.onDelete,
    // required this.progress,
  }) : super(key: key);

  final String image;
  final VoidCallback? onDelete;
  // final double progress;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: 75,
          height: 75,
          margin: EdgeInsets.only(top: 20, right: 20),
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: Colors.white10,
            ),
          ),
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              Image.network(image),
              // LinearProgressIndicator(
              //   minHeight: 2,
              //   value: progress / 100,
              // ),
            ],
          ),
        ),
        if (onDelete != null)
          Positioned(
            top: 10,
            right: 10,
            child: GestureDetector(
              onTap: onDelete,
              child: Container(
                padding: EdgeInsets.all(2),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50), color: Colors.red),
                child: Icon(Icons.close, size: 14),
              ),
            ),
          ),
      ],
    );
  }
}
