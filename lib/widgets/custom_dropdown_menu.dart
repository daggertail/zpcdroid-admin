import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

import '../theme.dart';

class CustomDropdownMenu extends StatefulWidget {
  CustomDropdownMenu({
    Key? key,
    required this.itemList,
    required this.onChanged,
    required this.hintText,
    this.validator,
    this.value,
  }) : super(key: key);

  final List<Map<String, dynamic>> itemList;

  final Function onChanged;
  final String hintText;
  dynamic value;
  String? Function(String?)? validator;

  @override
  State<CustomDropdownMenu> createState() => _CustomDropdownMenuState();
}

class _CustomDropdownMenuState extends State<CustomDropdownMenu> {
  bool focus = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(3),
        border: Border.all(color: focus ? primaryBlue : textGrey, width: 1),
        color: Colors.transparent,
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButtonFormField<String>(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: widget.validator,
          hint: Text(
            widget.hintText,
            style: TextStyle(fontWeight: FontWeight.w200),
          ),
          value: widget.value,
          items: widget.itemList.map((e) {
            return buildMenuItem(e);
          }).toList(),
          isExpanded: true,
          onChanged: (value) {
            setState(() {
              widget.value = value;
              return widget.onChanged(widget.value);
            });
          },
        ),
      ),
    );
  }

  DropdownMenuItem<String> buildMenuItem(Map<String, dynamic> item) {
    return DropdownMenuItem(
      value: item['value'],
      child: Text(item['label']),
    );
  }
}
