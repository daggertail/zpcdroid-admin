import 'package:flutter/material.dart';
import '../../../constants.dart';

class CustomInputField extends StatelessWidget {
  const CustomInputField({
    Key? key,
    required this.label,
    required this.child,
  }) : super(key: key);

  final String label;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: defaultPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label, textAlign: TextAlign.left),
          SizedBox(height: 5),
          child
        ],
      ),
    );
  }
}
