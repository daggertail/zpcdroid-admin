import 'package:flutter/material.dart';
import '../theme.dart';

import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';

import 'widgets.dart';

class CustomFileInput extends StatefulWidget {
  const CustomFileInput({Key? key}) : super(key: key);

  // var  onFileSelect

  @override
  State<CustomFileInput> createState() => _CustomFileInputState();
}

class _CustomFileInputState extends State<CustomFileInput> {
  List<XFile>? images;
  double progress = 0.0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: InkWell(
                  onTap: () {
                    _pickImage();
                  },
                  child: Container(
                    height: 50,
                    alignment: Alignment.center,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: textGrey, width: 1),
                      color: Colors.transparent,
                    ),
                    child: Text(
                      'Click to upload images...',
                      style: TextStyle(color: textGrey),
                    ),
                  ),
                ),
              ),
              SizedBox(width: defaultPadding),
              IconButton(
                onPressed: _uploadToCloud,
                icon: Icon(Icons.cloud_upload_outlined),
                iconSize: 36,
                hoverColor: Colors.transparent,
              )
            ],
          ),
          SizedBox(height: 10),
          if (images != null && images!.isNotEmpty)
            Wrap(children: [
              for (var i = 0; i < images!.length; i++)
                CustomImageThumbnail(
                    image: images![i].path,
                    // progress: progress,
                    onDelete: () {
                      setState(() {
                        images!.removeAt(i);
                      });
                    })
            ])
        ],
      ),
    );
  }

  Future<void> _pickImage() async {
    final ImagePicker _picker = ImagePicker();
    List<XFile>? pickedImages = await _picker.pickMultiImage();
    setState(() {
      images = pickedImages;
    });
  }

  Future<void> _uploadToCloud() async {
    for (var i = 0; i < images!.length; i++) {
      var task = FirebaseStorage.instance
          .ref()
          .child('images/${images![i].name}')
          // .putFile(File(images![i].path));
          .putData(
            await images![i].readAsBytes(),
            SettableMetadata(contentType: images![i].mimeType),
          );

      task.snapshotEvents.listen((event) async {
        setState(() {
          progress = ((event.bytesTransferred.toDouble() /
                      event.totalBytes.toDouble()) *
                  100)
              .roundToDouble();
        });

        print(await event.ref.getDownloadURL());
      });

      // print(e.metadata!.fullPath);
    }
  }
}
