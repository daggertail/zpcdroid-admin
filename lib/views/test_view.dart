import 'package:admin/theme.dart';
import 'package:flutter/material.dart';
import '../widgets/widgets.dart';
import '../responsive.dart';
import 'dart:io';

class TestView extends StatelessWidget {
  TestView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width / 2,
            padding: EdgeInsets.only(top: 30, bottom: 30),
            child: Column(
              children: [
                Text(
                  'Test View... For testing custom components...',
                  style: heading2,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: defaultPadding),
                // CustomFileInput()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
