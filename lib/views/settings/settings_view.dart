import 'dart:html';

import 'package:admin/responsive.dart';
import 'package:admin/views/auth/controller/auth_controller.dart';
import 'package:admin/widgets/widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../theme.dart';

class SettingsView extends GetWidget<AuthController> {
  const SettingsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();

    if (kIsWeb) {
      document.addEventListener('keydown',
          (event) => {if (event.type == 'tab') event.preventDefault()});
    }

    return SingleChildScrollView(
      padding: EdgeInsets.all(defaultPadding),
      child: Container(
        width: Responsive.isMobile(context)
            ? double.infinity
            : MediaQuery.of(context).size.width / 2,
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text('Change Password', style: heading2),
              SizedBox(height: defaultPadding * 2),
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Text('Old Password'),
                  ),
                  SizedBox(width: defaultPadding),
                  Expanded(
                    flex: 7,
                    child: CustomTextField(
                      obscureText: true,
                      controller: controller.currentPasswordController,
                      hintText: 'Current Password',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter your old password.';
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: defaultPadding),
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Text('New Password'),
                  ),
                  SizedBox(width: defaultPadding),
                  Expanded(
                    flex: 7,
                    child: CustomTextField(
                      obscureText: true,
                      controller: controller.newPasswordController,
                      hintText: 'New Password',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter your desired new password.';
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: defaultPadding),
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Text('Confirm Password'),
                  ),
                  SizedBox(width: defaultPadding),
                  Expanded(
                    flex: 7,
                    child: CustomTextField(
                      obscureText: true,
                      controller: controller.confirmPasswordController,
                      hintText: 'Confirm Password',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter again the new password.';
                        } else if (value == null ||
                            value != controller.newPasswordController.text) {
                          return 'Confirm password does not match with new password.';
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: defaultPadding * 2),
              Obx(
                () => CustomPrimaryButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        controller.checkPassword();
                      }
                    },
                    isLoading: controller.isLoading.value,
                    text: 'Change Password'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
