import 'package:admin/widgets/widgets.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import './controller/auth_controller.dart';
import '../views.dart';

class AuthWrapper extends GetWidget<AuthController> {
  const AuthWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // test your component inside testview
    // return TestView();
    return Obx(() => controller.user != null ? HomeView() : LoginView());
  }
}
