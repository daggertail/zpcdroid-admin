import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';
import 'package:get/get.dart';
import '../../theme.dart';
import '../../widgets/widgets.dart';
import '../../responsive.dart';
import './controller/auth_controller.dart';

class LoginView extends GetWidget<AuthController> {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _loginFormKey = GlobalKey<FormState>();

    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        padding: EdgeInsets.all(defaultPadding),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Center(
                child: Container(
                  width: Responsive.isDesktop(context)
                      ? MediaQuery.of(context).size.width / 3
                      : MediaQuery.of(context).size.width - 50,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(24.0, 40.0, 24.0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Welcome to ZPCDroid Admin',
                          style: heading2,
                        ),
                        SizedBox(
                          height: 48,
                        ),
                        Form(
                          key: _loginFormKey,
                          child: Column(
                            children: [
                              CustomTextField(
                                controller: controller.emailController,
                                hintText: 'Email',
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter an email.';
                                  } else if (!isEmail(value)) {
                                    return 'Please enter a valid email.';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                              SizedBox(
                                height: 32,
                              ),
                              Obx(
                                () => CustomTextField(
                                  hintText: 'Password',
                                  obscureText:
                                      !controller.isPasswordVisible.value,
                                  controller: controller.passwordController,
                                  suffixIcon: IconButton(
                                    color: Colors.white,
                                    splashRadius: 1,
                                    icon: Icon(
                                        !controller.isPasswordVisible.value
                                            ? Icons.visibility_outlined
                                            : Icons.visibility_off_outlined),
                                    onPressed: controller.togglePassword,
                                  ),
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter a password.';
                                    } else {
                                      return null;
                                    }
                                  },
                                  onFieldSubmitted: (value) {
                                    if (_loginFormKey.currentState!
                                        .validate()) {
                                      controller.loginUser();
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        // SizedBox(
                        //   height: 32,
                        // ),
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.start,
                        //   children: [
                        //     CustomCheckbox(),
                        //     SizedBox(
                        //       width: 12,
                        //     ),
                        //     Text('Remember me', style: regular16pt),
                        //   ],
                        // ),
                        SizedBox(
                          height: 32,
                        ),
                        Obx(
                          () => CustomPrimaryButton(
                            isLoading: controller.isLoading.value,
                            onPressed: () {
                              if (_loginFormKey.currentState!.validate()) {
                                controller.loginUser();
                              }
                            },
                            text: 'Login',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ]),
      )),
    );
  }
}
