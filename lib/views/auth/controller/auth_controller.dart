import 'package:admin/views/home/home_view.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

import '../login_view.dart';

class AuthController extends GetxController {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Rxn<User> _firebaseUser = Rxn<User>();
  final CollectionReference _userRef =
      FirebaseFirestore.instance.collection('users');

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  RxBool isPasswordVisible = false.obs;

  // password reset controller
  TextEditingController currentPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  final RxBool isLoading = false.obs;
  User? get user => _firebaseUser.value;
  String? get userId => _firebaseUser.value?.uid;

  @override
  void onInit() {
    _firebaseUser.bindStream(_auth.authStateChanges());
    super.onInit();
  }

  void togglePassword() {
    isPasswordVisible.value = !isPasswordVisible.value;
  }

  Future<void> createUser(
    String firstName,
    String lastName,
    String email,
    String password,
  ) async {
    try {
      var userValue = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      await _userRef
          .add({'firstName': firstName, 'lastName': lastName, 'email': email});
      Get.offAllNamed('login');
    } on FirebaseException catch (e) {}
  }

  Future<void> loginUser() async {
    try {
      isLoading.value = true;
      await _auth.signInWithEmailAndPassword(
          email: emailController.text, password: passwordController.text);

      // Get.offAll(() => HomeView());
    } on FirebaseException catch (e) {
      Get.snackbar(
        'Error',
        'Invalid email/password.',
        colorText: Colors.white,
        borderRadius: 0,
        backgroundColor: Colors.red[700],
      );
    } finally {
      isLoading.value = false;
    }
  }

  Future<void> checkPassword() async {
    try {
      isLoading.value = true;
      User? user = _auth.currentUser;
      var email = user!.email;

      if (email != null) {
        AuthCredential credential = EmailAuthProvider.credential(
            email: email, password: currentPasswordController.text);
        await _auth.currentUser!.reauthenticateWithCredential(credential);
        changePassword(newPassword: newPasswordController.text.trim());
      }
    } on FirebaseAuthException catch (e) {
      String errorString;
      switch (e.code) {
        case 'invalid-credential':
          errorString = 'Invalid credential provided.';
          break;
        case 'invalid-email':
          errorString =
              'The email of this user does not match within the record.';
          break;
        case 'wrong-password':
          errorString = 'The old password is invalid.';
          break;
        default:
          errorString = 'Something went wrong.';
      }
      Get.snackbar(
        'Error',
        errorString,
        colorText: Colors.white,
        borderRadius: 0,
        backgroundColor: Colors.red[700],
        duration: Duration(seconds: 2),
      );

      clearChangePasswordField();
    } catch (e) {
      Get.snackbar(
        'Error',
        e.toString(),
        colorText: Colors.white,
        borderRadius: 0,
        backgroundColor: Colors.red[700],
      );
      clearChangePasswordField();
    } finally {
      isLoading.value = false;
    }
  }

  void clearChangePasswordField() {
    currentPasswordController.text = '';
    newPasswordController.text = '';
    confirmPasswordController.text = '';
  }

  Future<void> changePassword({required String newPassword}) async {
    try {
      isLoading.value = true;
      User? user = _auth.currentUser;
      await _auth.currentUser!.updatePassword(newPassword);
      await signOut();

      Get.snackbar(
        'Success',
        'Password successfully changed.',
        colorText: Colors.white,
        borderRadius: 0,
        backgroundColor: Colors.green[700],
      );
    } on FirebaseException catch (e) {
      print(e);
    } finally {
      isLoading.value = false;
    }
  }

  Future<void> signInUser(String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      Get.offAllNamed('parts');
    } on FirebaseException catch (e) {}
  }

  Future<void> signInAnonymously() async {
    isLoading.value = true;
    try {
      await _auth.signInAnonymously();
      Get.offAllNamed('home');
    } on FirebaseException catch (e) {
    } finally {
      isLoading.value = false;
    }
  }

  Future<void> signOut() async {
    try {
      await _auth.signOut();
      emailController.text = '';
      passwordController.text = '';
      Get.offAll(() => LoginView());
    } catch (e) {}
  }
}
