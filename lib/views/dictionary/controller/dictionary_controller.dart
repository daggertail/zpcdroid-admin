import 'package:admin/controllers/utility_mixin.dart';
import 'package:admin/views/dictionary/components/term_form.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../models/dictionary_model.dart';
import '../../../db/dictionary_db.dart';

class DictionaryController extends GetxController with UtilityMixin {
  final RxList<Dictionary> dict = <Dictionary>[].obs;

  final RxList<Dictionary> searchedResults = <Dictionary>[].obs;
  final TextEditingController keywordTextController = TextEditingController();

  // Create form text controller
  final titleController = TextEditingController();
  final descController = TextEditingController();
  final citationsController = TextEditingController();

  // Id used for updating dictionary
  String? id;

  final RxBool isLoading = false.obs;

  @override
  void onInit() {
    dict.bindStream(DictionaryDB().getAll());
    searchedResults.value = dict;
    super.onInit();
  }

  search() {
    String keyword = keywordTextController.text.toLowerCase().trim();

    if (keyword.isNotEmpty) {
      RegExp exp = RegExp("\\b" + keyword + "\\b", caseSensitive: false);
      searchedResults.value =
          dict.where((e) => exp.hasMatch(e.title.toLowerCase())).toList();
    } else {
      searchedResults.value = dict;
    }
  }

  // Update methods
  void showUpdateFormModal(Dictionary data) {
    fillFormData(data);
    showFormDialog(
      title: 'Update Term',
      content: Padding(
        padding: const EdgeInsets.all(10.0),
        child: TermForm(term: data),
      ),
    );
  }

  void fillFormData(Dictionary data) {
    id = data.id;
    titleController.text = data.title;
    descController.text = data.desc;
    citationsController.text = data.citations!;
  }

  void clearFillFormData() {
    id = '';
    titleController.text = '';
    descController.text = '';
    citationsController.text = '';
  }

  void showFormModal() {
    clearFillFormData();
    showFormDialog(
      title: 'Create Term',
      content: Padding(
        padding: const EdgeInsets.all(10.0),
        child: TermForm(),
      ),
    );
  }

  Future<void> createTerm() async {
    isLoading.value = true;
    final data = {
      'title': titleController.text,
      'desc': descController.text,
      'citations': citationsController.text,
    };

    await DictionaryDB().createTerm(data);
    Get.back();

    searchedResults.value = dict;

    Get.snackbar(
      'Success',
      'Term succesfully created.',
      colorText: Colors.white,
      borderRadius: 0,
      backgroundColor: Colors.green[700],
    );
    isLoading.value = false;
  }

  Future<void> updateTerm() async {
    isLoading.value = true;

    await DictionaryDB().updateTerm({
      'id': id,
      'title': titleController.text,
      'desc': descController.text,
      'citations': citationsController.text
    });

    Get.back();
    searchedResults.value = dict;

    Get.snackbar(
      'Success',
      'Term succesfully updated.',
      colorText: Colors.white,
      borderRadius: 0,
      backgroundColor: Colors.green[700],
    );
    isLoading.value = false;
  }

  void showDeleteModal(String docId) {
    showModal(
      content: [
        Text('Are you sure you want to delete this term?'),
      ],
      title: 'Delete Term',
      confirmText: 'Delete',
      confirm: () {
        DictionaryDB().deleteTerm(docId);
        Get.back();
        Get.snackbar(
          'Success',
          'Term successfully deleted',
          colorText: Colors.white,
          borderRadius: 0,
          backgroundColor: Colors.green[700],
        );
      },
    );
  }
}
