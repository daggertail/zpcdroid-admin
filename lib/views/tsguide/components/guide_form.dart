import 'package:admin/models/tsguide_model.dart';
import 'package:admin/views/controllers.dart';
import 'package:admin/widgets/custom_input_field.dart';
import 'package:admin/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GuideForm extends GetWidget<TSGuideController> {
  GuideForm({Key? key, this.guide}) : super(key: key);

  final TSGuide? guide;

  @override
  Widget build(BuildContext context) {
    final _guideFormKey = GlobalKey<FormState>();

    return Container(
      width: MediaQuery.of(context).size.width / 3,
      child: Obx(
        () => Stack(
          alignment: Alignment.center,
          children: [
            if (controller.isLoading.value)
              Center(
                child: CircularProgressIndicator(),
              ),
            Opacity(
              opacity: controller.isLoading.value ? 0.3 : 1,
              child: Form(
                key: _guideFormKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomInputField(
                      label: 'Title',
                      child: CustomTextField(
                        controller: controller.titleController,
                        hintText: 'title',
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a title.';
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    CustomInputField(
                      label: 'Description',
                      child: CustomTextField(
                        controller: controller.descController,
                        hintText: 'description',
                      ),
                    ),
                    CustomInputField(
                      label: 'Source',
                      child: CustomTextField(
                        controller: controller.sourceController,
                        hintText: 'https://www.google.com',
                      ),
                    ),
                    CustomInputField(
                      label: 'Steps',
                      child: Obx(
                        () => Container(
                          height: MediaQuery.of(context).size.height * .35,
                          child: Column(
                            children: [
                              Expanded(
                                child: ListView.builder(
                                  itemCount: controller.stepsController.length,
                                  itemBuilder: (_, idx) => Container(
                                    margin:
                                        EdgeInsetsDirectional.only(bottom: 10),
                                    child: CustomTextField(
                                      suffixIcon: IconButton(
                                        onPressed: controller
                                                    .noMoreStep.value ==
                                                false
                                            ? () {
                                                controller.removeStepField(idx);
                                                print('remove step triggered');
                                              }
                                            : null,
                                        icon: Icon(Icons.close),
                                        iconSize: 12,
                                        color: Colors.red,
                                      ),
                                      controller:
                                          controller.stepsController[idx],
                                      hintText: 'Step #${idx + 1}',
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Please enter a step otherwise delete this field.';
                                        } else {
                                          return null;
                                        }
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              TextButton(
                                onPressed: () {
                                  controller.addStepField();
                                },
                                child: Text('Add More Step'),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 30),
                    Row(
                      children: [
                        Expanded(
                          child: CustomPrimaryButton(
                            buttonColor: Colors.grey,
                            onPressed: () {
                              Get.close(1);
                            },
                            text: 'Cancel',
                          ),
                        ),
                        SizedBox(width: 30),
                        Expanded(
                          child: CustomPrimaryButton(
                            // isLoading: controller.isLoading.value,
                            onPressed: () {
                              if (_guideFormKey.currentState!.validate()) {
                                if (guide != null) {
                                  controller.updateGuide();
                                } else {
                                  controller.createGuide();
                                }
                              }
                            },
                            text: (guide != null)
                                ? 'Update Guide'
                                : 'Create Guide',
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
