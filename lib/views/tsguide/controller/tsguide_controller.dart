import 'package:admin/controllers/utility_mixin.dart';
import 'package:admin/db/tsguide_db.dart';
import 'package:admin/models/tsguide_model.dart';
import 'package:admin/views/tsguide/components/guide_form.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TSGuideController extends GetxController with UtilityMixin {
  final RxList<TSGuide> tsguides = <TSGuide>[].obs;

  final keywordTextController = TextEditingController();
  final RxList<TSGuide> searchedResults = <TSGuide>[].obs;

  // Create form text controller
  final titleController = TextEditingController();
  final descController = TextEditingController();
  final sourceController = TextEditingController();

  final RxList<int> steps = <int>[].obs;
  final RxList<TextEditingController> stepsController = <TextEditingController>[
    ...List.generate(3, (index) => TextEditingController())
  ].obs;

  // Update form
  String? id;

  final RxBool noMoreStep = false.obs;
  final RxBool isLoading = false.obs;

  @override
  void onInit() {
    tsguides.bindStream(TSGuideDB().getAll());
    searchedResults.value = tsguides;
    super.onInit();
  }

  void addStepField() {
    stepsController.add(TextEditingController());

    if (stepsController.length > 1) {
      noMoreStep.value = false;
    }
  }

  void removeStepField(int index) {
    stepsController.removeAt(index);

    if (stepsController.length <= 1) {
      noMoreStep.value = true;
      return;
    }
  }

  void search() {
    String keyword = keywordTextController.text.toLowerCase().trim();

    if (keyword.isNotEmpty) {
      RegExp exp = RegExp("\\b" + keyword + "\\b", caseSensitive: false);
      searchedResults.value =
          tsguides.where((e) => exp.hasMatch(e.title.toLowerCase())).toList();
    } else {
      searchedResults.value = tsguides;
    }
  }

  void showUpdateFormModal(TSGuide data) {
    fillFormData(data);
    noMoreStep.value = false;
    showFormDialog(
      title: 'Update Guide',
      content: Padding(
        padding: const EdgeInsets.all(10.0),
        child: GuideForm(guide: data),
      ),
    );
  }

  void fillFormData(TSGuide data) {
    id = data.id;
    titleController.text = data.title;
    descController.text = data.desc;
    sourceController.text = data.source;

    // clear the prefilled element so it shows only the actual data
    stepsController.clear();

    data.steps.forEach((e) {
      stepsController.add(TextEditingController(text: e));
    });
  }

  void clearFillFormData() {
    id = '';
    titleController.text = '';
    descController.text = '';
    sourceController.text = '';
    stepsController.value = [
      ...List.generate(3, (index) => TextEditingController())
    ];
  }

  void showFormModal() {
    clearFillFormData();
    noMoreStep.value = false;
    showFormDialog(
      title: 'Create Guide',
      content: Padding(
        padding: const EdgeInsets.all(10.0),
        child: GuideForm(),
      ),
    );
  }

  Future<void> createGuide() async {
    isLoading.value = true;
    final data = {
      'title': titleController.text,
      'desc': descController.text,
      'source': sourceController.text,
      'steps': stepsController.map((val) => val.text).toList(),
    };

    await TSGuideDB().createGuide(data);
    Get.back();

    searchedResults.value = tsguides;

    Get.snackbar(
      'Success',
      'Guide succesfully created.',
      colorText: Colors.white,
      borderRadius: 0,
      backgroundColor: Colors.green[700],
    );
    isLoading.value = false;
  }

  Future<void> updateGuide() async {
    isLoading.value = true;

    await TSGuideDB().updateGuide({
      'id': id,
      'title': titleController.text,
      'desc': descController.text,
      'source': sourceController.text,
      'steps': stepsController.map((val) => val.text).toList(),
    });

    Get.back();

    searchedResults.value = tsguides;

    Get.snackbar(
      'Success',
      'Guide succesfully updated.',
      colorText: Colors.white,
      borderRadius: 0,
      backgroundColor: Colors.green[700],
    );
    isLoading.value = false;
  }

  void showDeleteModal(String docId) {
    showModal(
      content: [
        Text('Are you sure you want to delete this guide?'),
      ],
      title: 'Delete Guide',
      confirmText: 'Delete',
      confirm: () {
        TSGuideDB().delete(docId);
        Get.back();
        Get.snackbar(
          'Success',
          'Guide successfully deleted',
          colorText: Colors.white,
          borderRadius: 0,
          backgroundColor: Colors.green[700],
        );
      },
    );
  }

  void updateDB() async {
    await TSGuideDB().updateDB();
    showSnackbar('success', 'Successfully updated', 'DB successfully updated');
  }
}
