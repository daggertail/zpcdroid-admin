import 'package:admin/models/dictionary_model.dart';
import 'package:admin/models/sales_model.dart';
import 'package:admin/views/controllers.dart';
import 'package:admin/widgets/custom_input_field.dart';
import 'package:admin/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SalesForm extends GetWidget<SalesController> {
  SalesForm({Key? key, this.sales}) : super(key: key);

  final Sales? sales;

  @override
  Widget build(BuildContext context) {
    final _salesFormKey = GlobalKey<FormState>();

    return Container(
      width: MediaQuery.of(context).size.width / 3,
      child: Obx(
        () => Stack(
          alignment: Alignment.center,
          children: [
            if (controller.isLoading.value)
              Center(
                child: CircularProgressIndicator(),
              ),
            Opacity(
              opacity: controller.isLoading.value ? 0.3 : 1,
              child: Form(
                key: _salesFormKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomInputField(
                      label: 'Title',
                      child: CustomTextField(
                        isReadOnly: true,
                        controller: controller.titleController,
                        hintText: 'title',
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a title.';
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    CustomInputField(
                      label: 'Customer Name',
                      child: CustomTextField(
                        isReadOnly: true,
                        controller: controller.customerName,
                        hintText: 'description',
                      ),
                    ),
                    SizedBox(height: 30),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        // Expanded(
                        //   child: CustomInputField(
                        //     label: 'Sale Status',
                        //     child: CustomDropdownMenu(
                        //       hintText: 'Select Status',
                        //       validator: (value) {
                        //         if (value == null) {
                        //           return 'Sale status is required.';
                        //         } else {
                        //           return null;
                        //         }
                        //       },
                        //       itemList: [
                        //         {
                        //           'New': 'New',
                        //           'Processing': 'Processing',
                        //           'Fullfilled': 'FullFilled'
                        //         }
                        //       ],
                        //       value: sale.status,
                        //       onChanged: (value) {
                        //         controller.spec['mem_type'] = value;
                        //       },
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: CustomPrimaryButton(
                            buttonColor: Colors.grey,
                            onPressed: () {
                              Get.close(1);
                            },
                            text: 'Cancel',
                          ),
                        ),
                        SizedBox(width: 30),
                        Expanded(
                          child: CustomPrimaryButton(
                            // isLoading: controller.isLoading.value,
                            onPressed: () {
                              if (_salesFormKey.currentState!.validate()) {
                                if (sales != null) {
                                  controller.updateTerm();
                                }
                              }
                            },
                            text: 'Update Sale',
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
