import 'package:admin/controllers/utility_mixin.dart';
import 'package:admin/db/sales_db.dart';
import 'package:admin/models/sales_model.dart';
import 'package:admin/views/dictionary/components/term_form.dart';
import 'package:admin/views/sales/components/sales_form.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SalesController extends GetxController with UtilityMixin {
  final RxList<Sales> sales = <Sales>[].obs;

  final RxList<Sales> searchedResults = <Sales>[].obs;
  final TextEditingController keywordTextController = TextEditingController();

  // Create form text controller
  final titleController = TextEditingController();
  final customerName = TextEditingController();

  // Id used for updating dictionary
  String? id;

  final RxBool isLoading = false.obs;

  @override
  void onInit() {
    sales.bindStream(SalesDB().getAllSales());
    searchedResults.value = sales;
    super.onInit();
  }

  search() {
    String keyword = keywordTextController.text.toLowerCase().trim();

    if (keyword.isNotEmpty) {
      RegExp exp = RegExp("\\b" + keyword + "\\b", caseSensitive: false);
      searchedResults.value =
          sales.where((e) => exp.hasMatch(e.title.toLowerCase())).toList();
    } else {
      searchedResults.value = sales;
    }
  }

  // Update methods
  void showUpdateFormModal(Sales data) {
    fillFormData(data);
    showFormDialog(
      title: 'Update Term',
      content: Padding(
        padding: const EdgeInsets.all(10.0),
        child: SalesForm(sales: data),
      ),
    );
  }

  void fillFormData(Sales data) {
    id = data.id;
    titleController.text = data.title;
    customerName.text = data.customerName!;
  }

  void clearFillFormData() {
    id = '';
    titleController.text = '';
    customerName.text = '';
  }

  void showFormModal() {
    clearFillFormData();
    showFormDialog(
      title: 'Create Term',
      content: Padding(
        padding: const EdgeInsets.all(10.0),
        child: TermForm(),
      ),
    );
  }

  Future<void> updateTerm() async {
    isLoading.value = true;

    // await DictionaryDB().updateTerm({
    //   'id': id,
    //   'title': titleController.text,
    //   'desc': descController.text,
    // });

    Get.back();

    Get.snackbar(
      'Success',
      'Term succesfully updated.',
      colorText: Colors.white,
      borderRadius: 0,
      backgroundColor: Colors.green[700],
    );
    isLoading.value = false;
  }

  void showDeleteModal(String docId) {
    showModal(
      content: [
        Text('Are you sure you want to delete this sale?'),
      ],
      title: 'Delete Sale',
      confirmText: 'Delete',
      confirm: () {
        SalesDB().delete(docId);
        Get.back();
        Get.snackbar(
          'Success',
          'Sale successfully deleted',
          colorText: Colors.white,
          borderRadius: 0,
          backgroundColor: Colors.green[700],
        );
      },
    );
  }
}
