import 'dart:async';

import 'package:admin/db/sales_db.dart';
import 'package:admin/models/sales_model.dart';
import 'package:admin/views/controllers.dart';
import 'package:admin/widgets/search_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../constants.dart';

class SalesView extends GetWidget<SalesController> {
  const SalesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Timer _debounce;

    return SingleChildScrollView(
      padding: EdgeInsets.all(defaultPadding),
      child: Container(
        width: double.infinity,
        child: Obx(
          () => Column(
            children: [
              Wrap(
                runSpacing: 20,
                children: [
                  SearchField(
                    searchFieldController: controller.keywordTextController,
                    onChanged: (_) {
                      _debounce = Timer(const Duration(milliseconds: 1000), () {
                        controller.search();
                      });
                    },
                  )
                ],
              ),
              controller.searchedResults.isEmpty
                  ? Container(
                      child: Center(child: CircularProgressIndicator()),
                      height: MediaQuery.of(context).size.height - 200)
                  : Container(
                      margin: EdgeInsets.only(top: 20),
                      height: MediaQuery.of(context).size.height - 200,
                      child: ListView.builder(
                        itemCount: controller.searchedResults.length,
                        itemBuilder: (_, idx) => ListTile(
                          leading: Chip(
                            label: Text(controller.searchedResults[idx].status),
                            backgroundColor: Colors.green,
                          ),
                          contentPadding: EdgeInsets.only(bottom: 10),
                          visualDensity: VisualDensity.comfortable,
                          horizontalTitleGap: 30,
                          title: Text(controller.searchedResults[idx].title),
                          subtitle: Text(
                            'Date: ${Sales.convertTimeStamp(controller.searchedResults[idx].updatedOn)}',
                            style: TextStyle(color: Colors.grey[600]),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              IconButton(
                                hoverColor: Colors.transparent,
                                onPressed: () {
                                  controller.showUpdateFormModal(
                                      controller.searchedResults[idx]);
                                },
                                icon: Icon(
                                  Icons.settings,
                                  color: Colors.blue,
                                ),
                              ),
                              SizedBox(width: defaultPadding),
                              IconButton(
                                hoverColor: Colors.transparent,
                                onPressed: () {
                                  controller.showDeleteModal(
                                      controller.searchedResults[idx].id);
                                },
                                icon: Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
            ],
          ),
        ),
      ),
    );
  }
}
