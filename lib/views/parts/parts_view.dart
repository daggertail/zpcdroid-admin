import 'dart:async';

import 'package:admin/models/category_model.dart';
import 'package:admin/responsive.dart';
import 'package:admin/widgets/search_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers.dart';

import '../../constants.dart';

class PartsView extends GetWidget<PartController> {
  @override
  Widget build(BuildContext context) {
    final List<Category> categoryList = [
      Category(name: 'All', type: 'All'),
      Category(name: 'CPU', type: 'CPU'),
      Category(name: 'Motherboard', type: 'Motherboard'),
      Category(name: 'RAM', type: 'RAM'),
      Category(name: 'GPU', type: 'GPU'),
      Category(name: 'Storage', type: 'Storage'),
      Category(name: 'PSU', type: 'PSU'),
      Category(name: 'CPU Cooling', type: 'CPU Cooling'),
      Category(name: 'Casing', type: 'Casing'),
      Category(name: 'Fans', type: 'Fans'),
      Category(name: 'Monitor', type: 'Monitor'),
      Category(name: 'Mouse', type: 'Mouse'),
      Category(name: 'Keyboard', type: 'Keyboard'),
      Category(name: 'Audio Device', type: 'Audio Device'),
    ];

    Timer _debounce;

    return SingleChildScrollView(
      padding: EdgeInsets.all(defaultPadding),
      child: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Obx(() => Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  runSpacing: 20,
                  children: [
                    MaterialButton(
                      onPressed: () {
                        controller.showCategorySelectionModal();
                      },
                      child: Text('Add New Part'),
                      height: 55,
                      minWidth: ButtonTheme.of(context).minWidth + 50,
                      color: Colors.blue,
                    ),
                    SizedBox(width: 20),
                    ...List.generate(categoryList.length, (index) {
                      return CategoryButton(
                          title: categoryList[index].name,
                          isSelected: controller.selectedCategory ==
                                  categoryList[index].type
                              ? true
                              : false,
                          onPressed: () {
                            controller.changeCategory(categoryList[index].type);
                          });
                    }),
                    SearchField(
                      searchFieldController: controller.searchFieldController,
                      onChanged: (_) {
                        _debounce =
                            Timer(const Duration(milliseconds: 1000), () {
                          controller.search();
                        });
                      },
                    )
                  ],
                )),
            Column(
              children: [
                Container(
                  width: double.infinity,
                  child: Obx(
                    () => controller.searchResults.isEmpty
                        ? Container(
                            child: Center(child: CircularProgressIndicator()),
                            height: MediaQuery.of(context).size.height - 200)
                        : Container(
                            margin: EdgeInsets.only(top: 20),
                            height: MediaQuery.of(context).size.height - 200,
                            child: ListView.builder(
                              itemCount: controller.searchResults.length,
                              itemBuilder: (_, idx) => ListTile(
                                contentPadding: EdgeInsets.only(bottom: 10),
                                visualDensity: VisualDensity.comfortable,
                                horizontalTitleGap: 30,
                                leading: controller
                                        .searchResults[idx].images.isNotEmpty
                                    ? Image.network(
                                        controller.searchResults[idx].images[0],
                                        width: 50,
                                        fit: BoxFit.fill,
                                      )
                                    : Image.network(
                                        'https://dummyimage.com/600x400/2e2e2e/ffffff&text=Parts+Image+Here',
                                        width: 50,
                                        fit: BoxFit.fill,
                                      ),
                                title: Text(controller.searchResults[idx].name),
                                subtitle: Text(
                                  controller.searchResults[idx].category,
                                  style: TextStyle(color: Colors.grey[600]),
                                ),
                                trailing: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    IconButton(
                                      hoverColor: Colors.transparent,
                                      onPressed: () {
                                        controller.showUpdateFormModal(
                                            controller.searchResults[idx]);
                                      },
                                      icon: Icon(
                                        Icons.edit,
                                        color: Colors.blue,
                                      ),
                                    ),
                                    SizedBox(width: defaultPadding),
                                    IconButton(
                                      hoverColor: Colors.transparent,
                                      onPressed: () {
                                        controller.showDeleteModal(
                                            controller.searchResults[idx].id);
                                      },
                                      icon: Icon(
                                        Icons.delete,
                                        color: Colors.red,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                  ),
                )
              ],
            ),
            if (!Responsive.isMobile(context)) SizedBox(width: defaultPadding)
          ],
        ),
      ),
    );
  }
}

class CategoryButton extends StatelessWidget {
  final String title;
  final VoidCallback? onPressed;
  final bool isSelected;

  const CategoryButton({
    Key? key,
    this.title = '',
    this.onPressed,
    this.isSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool _isSelected = isSelected;

    return isSelected
        ? ElevatedButton(
            child: Text(title, style: const TextStyle(color: Colors.white)),
            style: ElevatedButton.styleFrom(
              onSurface: Colors.white,
              textStyle: const TextStyle(fontSize: 14.0),
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 0),
            ),
            onPressed: _isSelected ? null : onPressed,
          )
        : TextButton(
            child: Text(
              title,
              style: const TextStyle(color: Colors.white),
            ),
            style: TextButton.styleFrom(
              textStyle: const TextStyle(fontSize: 14.0),
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 0),
            ),
            onPressed: onPressed,
          );
  }
}
