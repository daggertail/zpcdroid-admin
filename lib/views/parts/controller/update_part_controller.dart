import 'package:admin/db/part_db.dart';
import 'package:admin/models/part_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import 'package:path/path.dart' as p;

class UpdatePartController extends GetxController {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController priceController = TextEditingController();
  final TextEditingController descController = TextEditingController();
  String category = '';

  final socketList = [].obs;

  final RxString selectedArch = ''.obs;
  final RxString selectedSocket = ''.obs;

  final RxMap<String, dynamic> spec = <String, dynamic>{}.obs;

  String? id;

  RxBool isLoading = false.obs;

  RxList<dynamic> loadedImages = [].obs;
  List<String> imageUrlToSave = <String>[].obs;

  void fillFormData(Part data) {
    id = data.id;
    nameController.text = data.name;
    priceController.text = data.price;
    descController.text = data.desc;
    category = data.category;
    spec.value = data.spec!;
    loadedImages.value = data.images;
  }

  Future<void> pickImage() async {
    final ImagePicker _picker = ImagePicker();
    loadedImages.value = (await _picker.pickMultiImage())!;
  }

  void deleteThumbnail(int idx) {
    loadedImages.removeAt(idx);
  }

  Future<void> updatePart() async {
    isLoading.value = true;

    await PartDB().updatePart({
      'id': id,
      'name': nameController.text,
      'category': category,
      'price': priceController.text,
      'desc': descController.text,
      'spec': spec,
    }, loadedImages);

    Get.back();

    Get.snackbar(
      'Success',
      'Part succesfully updated.',
      duration: Duration(seconds: 1),
    );
    isLoading.value = false;
  }
}
