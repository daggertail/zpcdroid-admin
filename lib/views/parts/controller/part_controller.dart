import 'package:admin/controllers/utility_mixin.dart';
import 'package:admin/views/parts/components/part_form.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import '../../../db/part_db.dart';
import '../../../models/part_model.dart';
import '../components/category_selection.dart';

class PartController extends GetxController with UtilityMixin {
  final RxList<Part> parts = <Part>[].obs;

  final searchFieldController = TextEditingController();

  final RxString _selectedCategory = 'All'.obs;
  get selectedCategory => _selectedCategory;
  final RxBool isLoading = false.obs;

  // final RxString searchKey = ''.obs;
  final RxList<Part> searchResults = <Part>[].obs;

  // create form
  RxList<XFile>? pickedImages = <XFile>[].obs;

  // update form
  final TextEditingController nameController = TextEditingController();
  final TextEditingController priceController = TextEditingController();
  final TextEditingController descController = TextEditingController();
  final RxMap<String, dynamic> spec = <String, dynamic>{}.obs;
  RxList<dynamic> loadedImages = [].obs;
  List<String> imageUrlToSave = <String>[].obs;
  String category = '';
  String? id;

  // competitor's price form
  final RxList<int> compeFormCount = <int>[].obs;
  final RxList<Map<String, TextEditingController>> compeControllers =
      <Map<String, TextEditingController>>[
    ...List.generate(
        2,
        (index) => {
              'storeName': TextEditingController(),
              'link': TextEditingController(),
              'price': TextEditingController()
            })
  ].obs;

  final RxBool noMoreCompeForm = false.obs;

  @override
  void onInit() {
    parts.bindStream(PartDB().getAllParts());
    searchResults.value = parts;
    super.onInit();
  }

  void addCompeForm() {
    compeControllers.add({
      'storeName': TextEditingController(),
      'link': TextEditingController(),
      'price': TextEditingController()
    });

    if (compeControllers.length > 1) {
      noMoreCompeForm.value = false;
    }
  }

  void removeCompeForm(int index) {
    compeControllers.removeAt(index);

    if (compeControllers.length <= 1) {
      noMoreCompeForm.value = true;
      return;
    }
  }

  void showCategorySelectionModal() {
    clearFillFormData();
    noMoreCompeForm.value = false;
    showFormDialog(
      title: 'Choose Category',
      content: Padding(
        padding: const EdgeInsets.all(10.0),
        child: CategorySelection(),
      ),
    );
  }

  void showUpdateFormModal(Part part) {
    fillFormData(part);
    noMoreCompeForm.value = false;
    showFormDialog(
      title: 'Update ${part.name}',
      content: Padding(
        padding: const EdgeInsets.all(10.0),
        child: PartForm(part: part, category: part.category),
      ),
    );
  }

  void fillFormData(Part data) {
    id = data.id;
    nameController.text = data.name;
    priceController.text = data.price;
    descController.text = data.desc;
    category = data.category;
    spec.value = data.spec!;
    loadedImages.value = data.images;

    pickedImages?.clear();
    compeControllers.clear();

    data.competitorsPrice!.forEach((e) {
      compeControllers.add({
        'storeName': TextEditingController(text: e['storeName']),
        'link': TextEditingController(text: e['link']),
        'price': TextEditingController(text: e['price'])
      });
    });
  }

  void clearFillFormData() {
    id = '';
    category = '';
    nameController.text = '';
    priceController.text = '';
    descController.text = '';
    spec.value = {};
    pickedImages?.clear();
    loadedImages.clear();

    compeControllers.value = [
      ...List.generate(
          2,
          (index) => {
                'storeName': TextEditingController(),
                'link': TextEditingController(),
                'price': TextEditingController()
              })
    ];
  }

  void showFormModal(String category) {
    Widget formWidget;

    switch (category) {
      case 'CPU':
        formWidget = PartForm(category: 'CPU');
        break;
      case 'Motherboard':
        formWidget = PartForm(category: 'Motherboard');
        break;
      case 'RAM':
        formWidget = PartForm(category: 'RAM');
        break;
      default:
        formWidget = PartForm(category: category);
    }
    Get.close(1);
    showFormDialog(
      title: 'Create $category',
      content: Padding(
        padding: const EdgeInsets.all(10.0),
        child: formWidget,
      ),
    );
  }

  getPartsByCategory(String categoryName) {
    try {
      isLoading.value = true;
      parts.bindStream(PartDB().getAllPartsByCategory(categoryName));
    } finally {
      isLoading.value = false;
    }
  }

  void search() {
    String keyword = searchFieldController.text.toLowerCase().trim();

    if (keyword.isNotEmpty) {
      RegExp exp = RegExp("\\b" + keyword + "\\b", caseSensitive: false);
      searchResults.value =
          parts.where((e) => exp.hasMatch(e.name.toLowerCase())).toList();
    } else {
      searchResults.value = parts;
    }
  }

  // void clearSearch() {
  //   searchedParts.value = [];
  //   searchKey.value = '';
  // }

  getAllParts() {
    try {
      isLoading.value = true;
      parts.bindStream(PartDB().getAllParts());
    } finally {
      isLoading.value = false;
    }
  }

  void changeCategory(String name) {
    _selectedCategory.value = name;

    if (_selectedCategory.value == 'All') {
      getAllParts();
    } else {
      getPartsByCategory(name);
    }
  }

  void showDeleteModal(String docId) {
    showModal(
      content: [
        Text('Are you sure you want to delete this part?'),
      ],
      title: 'Delete Part',
      confirmText: 'Delete',
      confirm: () {
        PartDB().delete(docId);
        Get.back();
        Get.snackbar(
          'Success',
          'Part successfully deleted',
          colorText: Colors.white,
          borderRadius: 0,
          backgroundColor: Colors.green[700],
        );
      },
    );
  }

  Future<void> pickImage() async {
    final ImagePicker _picker = ImagePicker();
    pickedImages!.value = (await _picker.pickMultiImage())!;
  }

  void deleteThumbnail(int idx) {
    pickedImages!.removeAt(idx);
  }

  Future<void> createPart(String category) async {
    isLoading.value = true;
    await PartDB().createPart({
      'name': nameController.text,
      'category': category,
      'price': priceController.text,
      'desc': descController.text,
      'spec': spec,
      'competitorsPrice': compeControllers.map((v) => {
            'storeName': v['storeName']!.text,
            'link': v['link']!.text,
            'price': v['price']!.text,
          }),
    }, pickedImages!);

    Get.back();

    searchResults.value = parts;

    Get.snackbar(
      'Success',
      'Part succesfully created.',
      duration: Duration(seconds: 1),
    );
    isLoading.value = false;
  }

  Future<void> updatePart() async {
    isLoading.value = true;

    await PartDB().updatePart({
      'id': id,
      'name': nameController.text,
      'category': category,
      'price': priceController.text,
      'desc': descController.text,
      'spec': spec,
      'competitorsPrice': compeControllers.map((v) => {
            'storeName': v['storeName']!.text,
            'link': v['link']!.text,
            'price': v['price']!.text,
          }),
    }, loadedImages);

    Get.back();

    searchResults.value = parts;

    Get.snackbar(
      'Success',
      'Part succesfully updated.',
      duration: Duration(seconds: 1),
    );
    isLoading.value = false;
  }
}
