import 'package:admin/views/controllers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../theme.dart';
import 'category_box.dart';

class CategorySelection extends GetWidget<PartController> {
  const CategorySelection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<CategoryLink> categoryList = [
      CategoryLink(
        text: 'CPU',
        svgAsset: 'assets/icons/cpu.png',
        category: 'CPU',
      ),
      CategoryLink(
        text: 'Motherboard',
        svgAsset: 'assets/icons/motherboard.png',
        category: 'Motherboard',
      ),
      CategoryLink(
        text: 'RAM',
        svgAsset: 'assets/icons/ram.png',
        category: 'RAM',
      ),
      CategoryLink(
        text: 'Storage',
        svgAsset: 'assets/icons/hdd.png',
        category: 'Storage',
      ),
      CategoryLink(
        text: 'PSU',
        svgAsset: 'assets/icons/powersupply.png',
        category: 'PSU',
      ),
      CategoryLink(
        text: 'GPU',
        svgAsset: 'assets/icons/videocard.png',
        category: 'GPU',
      ),
      CategoryLink(
        text: 'Casing',
        svgAsset: 'assets/icons/case.png',
        category: 'Casing',
      ),
      CategoryLink(
        text: 'Cooler',
        svgAsset: 'assets/icons/cpucooler.png',
        category: 'CPU Cooling',
      ),
      CategoryLink(
        text: 'Fan',
        svgAsset: 'assets/icons/fan.png',
        category: 'Fans',
      ),
      CategoryLink(
        text: 'Monitor',
        svgAsset: 'assets/icons/monitor.png',
        category: 'Monitor',
      ),
      CategoryLink(
        text: 'Mouse',
        svgAsset: 'assets/icons/mouse.png',
        category: 'Mouse',
      ),
      CategoryLink(
        text: 'Keyboard',
        svgAsset: 'assets/icons/keyboard.png',
        category: 'Keyboard',
      ),
      CategoryLink(
        text: 'Audio Device',
        svgAsset: 'assets/icons/audio.png',
        category: 'Audio Device',
      ),
    ];

    return Container(
      width: MediaQuery.of(context).size.width / 2, // check device width
      padding: EdgeInsets.all(defaultPadding),
      child: Wrap(
        runSpacing: 20,
        spacing: 20,
        alignment: WrapAlignment.center,
        children: categoryList
            .map((v) => CategoryBox(
                  text: v.text,
                  svgAsset: v.svgAsset,
                  onPressed: () {
                    controller.showFormModal(v.category);
                  },
                ))
            .toList(),
      ),
    );
  }
}

class CategoryLink {
  final String text;
  final String svgAsset;
  final String category;

  const CategoryLink({
    required this.text,
    required this.svgAsset,
    required this.category,
  });
}
