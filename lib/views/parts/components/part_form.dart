import 'package:admin/models/part_model.dart';
import 'package:admin/views/controllers.dart';
import 'package:admin/widgets/custom_input_field.dart';
import 'package:admin/views/parts/components/spec_input_mixin.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:validators/validators.dart';
import '../../../constants.dart';
import '../../../widgets/widgets.dart';

class PartForm extends GetWidget<PartController> with SpecInputMixin {
  PartForm({Key? key, required this.category, this.part}) : super(key: key);

  final String category;
  final Part? part;

  @override
  Widget build(BuildContext context) {
    final _partFormKey = GlobalKey<FormState>();

    return Container(
      width: MediaQuery.of(context).size.width / 3,
      height: MediaQuery.of(context).size.height - 150,
      child: Obx(
        () => Stack(
          alignment: Alignment.center,
          children: [
            if (controller.isLoading.value)
              Center(
                child: CircularProgressIndicator(),
              ),
            Opacity(
              opacity: controller.isLoading.value ? 0.3 : 1,
              child: Form(
                key: _partFormKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: CustomInputField(
                            label: 'Name',
                            child: CustomTextField(
                              controller: controller.nameController,
                              hintText: 'Name',
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter a name.';
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                        ),
                        SizedBox(width: defaultPadding),
                        Expanded(
                            child: CustomInputField(
                          label: 'Price',
                          child: CustomTextField(
                            controller: controller.priceController,
                            hintText: 'Price',
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter a price.';
                              } else if (!isNumeric(value)) {
                                return 'Please enter a numeric number.';
                              } else {
                                return null;
                              }
                            },
                          ),
                        )),
                      ],
                    ),
                    CustomInputField(
                      label: 'Description',
                      child: CustomTextField(
                        controller: controller.descController,
                        hintText: 'Description',
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a description.';
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    if (part != null)
                      Obx(() {
                        return Wrap(
                          children: controller.loadedImages
                              .asMap()
                              .entries
                              .map(
                                (entry) => CustomImageThumbnail(
                                  image: entry.value is XFile
                                      ? entry.value.path
                                      : entry.value,
                                ),
                              )
                              .toList(),
                        );
                      }),
                    if (part == null)
                      CustomInputField(
                        label: 'Images',
                        child: CustomImageField(
                          onPressed: controller.pickImage,
                        ),
                      ),
                    SizedBox(height: 10),
                    Obx(() {
                      var images = controller.pickedImages;
                      if (images != null && images.isNotEmpty)
                        return Wrap(children: [
                          for (var i = 0; i < images.length; i++)
                            CustomImageThumbnail(
                              image: images[i].path,
                              // progress: progress,
                              onDelete: () {
                                controller.deleteThumbnail(i);
                              },
                            ),
                        ]);
                      else {
                        return SizedBox(height: 0);
                      }
                    }),
                    SizedBox(height: 10),
                    buildSpecFieldInput(
                        category: category, controller: controller),
                    SizedBox(height: 10),
                    // Compeform Fields
                    Expanded(
                      child: ListView.builder(
                        itemCount: controller.compeControllers.length,
                        itemBuilder: (_, idx) => _buildCompetitorsFormField(
                            controller.compeControllers[idx], idx),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        controller.addCompeForm();
                      },
                      child: Text('Add More Store'),
                    ),
                    SizedBox(height: 30),
                    Row(
                      children: [
                        Expanded(
                          child: CustomPrimaryButton(
                            buttonColor: Colors.grey,
                            onPressed: () {
                              Get.close(1);
                            },
                            text: 'Cancel',
                          ),
                        ),
                        SizedBox(width: 30),
                        Expanded(
                          child: CustomPrimaryButton(
                            isLoading: controller.isLoading.value,
                            onPressed: () {
                              if (_partFormKey.currentState!.validate()) {
                                if (part != null) {
                                  controller.updatePart();
                                } else {
                                  controller.createPart(category);
                                }
                              }
                            },
                            text:
                                (part != null) ? 'Update Part' : 'Create Part',
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCompetitorsFormField(
      Map<String, TextEditingController> compeForm, int idx) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: CustomInputField(
                label: 'Store Name',
                child: CustomTextField(
                  controller: compeForm['storeName']!,
                  hintText: 'Name',
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the name of the store.';
                    } else {
                      return null;
                    }
                  },
                ),
              ),
            ),
            SizedBox(width: defaultPadding),
            Expanded(
              flex: 2,
              child: CustomInputField(
                label: 'Link',
                child: CustomTextField(
                  controller: compeForm['link']!,
                  hintText: 'Link of Store',
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the link of the part.';
                    } else {
                      return null;
                    }
                  },
                ),
              ),
            ),
            SizedBox(width: defaultPadding),
            Expanded(
              flex: 1,
              child: CustomInputField(
                label: 'Price',
                child: CustomTextField(
                  controller: compeForm['price']!,
                  hintText: 'Price',
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter a price.';
                    } else if (!isNumeric(value)) {
                      return 'Please enter a numeric number.';
                    } else {
                      return null;
                    }
                  },
                ),
              ),
            ),
          ],
        ),
        Obx(() => GestureDetector(
              child: Text(
                'Delete',
                style: TextStyle(
                  fontSize: 10,
                  color: Colors.red,
                ),
              ),
              onTap: controller.noMoreCompeForm.value == false
                  ? () {
                      controller.removeCompeForm(idx);
                      print('remove form triggered');
                    }
                  : null,
            ))
      ],
    );
  }
}
