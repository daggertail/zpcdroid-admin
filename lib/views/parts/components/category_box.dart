import 'package:flutter/material.dart';
import 'package:admin/theme.dart';

class CategoryBox extends StatelessWidget {
  const CategoryBox({
    Key? key,
    required this.text,
    required this.svgAsset,
    required this.onPressed,
  }) : super(key: key);

  final String text;
  final String svgAsset;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        width: 140,
        height: 140,
        padding: EdgeInsets.all(10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(color: Colors.white30),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              svgAsset,
              width: 40,
            ),
            SizedBox(height: defaultPadding),
            Text(text)
          ],
        ),
      ),
    );
  }
}
