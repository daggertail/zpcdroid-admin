import '../../../theme.dart';
import '../../../widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../widgets/custom_input_field.dart';

class SpecInputMixin {
  Widget buildSpecFieldInput({
    required String category,
    required dynamic controller,
  }) {
    final List<Map<String, dynamic>> _archList = [
      {
        'value': 'intel',
        'label': 'Intel',
      },
      {
        'value': 'AMD',
        'label': 'AMD',
      },
    ];

    final List<Map<String, dynamic>> _socketList = [
      {'value': 'LGA 2066', 'label': 'LGA 2066'},
      {'value': 'LGA 1200', 'label': 'LGA 1200'},
      {'value': 'LGA 1151', 'label': 'LGA 1151'},
      {'value': 'LGA 1150', 'label': 'LGA 1150'},
      {'value': 'sTRX4', 'label': 'sTRX4'},
      {'value': 'TR4', 'label': 'TR4'},
      {'value': 'AM4', 'label': 'AM4'},
      {'value': 'AM3+', 'label': 'AM3+'},
      {'value': 'AM3', 'label': 'AM3'},
      {'value': 'FM2+', 'label': 'FM2+'},
      {'value': 'FM2', 'label': 'FM2'},
    ];

    final List<Map<String, dynamic>> _memList = [
      {'value': 'DDR4', 'label': 'DDR4'},
      {'value': 'DDR3', 'label': 'DDR3'},
      {'value': 'DDR2', 'label': 'DDR2'},
    ];

    if (category == 'CPU') {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: CustomInputField(
              label: 'Architecture',
              child: CustomDropdownMenu(
                hintText: 'Select CPU Architecture',
                validator: (value) {
                  if (value == null) {
                    return 'CPU Architecture is required.';
                  } else {
                    return null;
                  }
                },
                itemList: _archList,
                value: controller.spec['arch'],
                onChanged: (value) {
                  controller.spec['arch'] = value;
                },
              ),
            ),
          ),
          SizedBox(width: defaultPadding),
          Expanded(
            child: CustomInputField(
              label: 'Socket',
              child: CustomDropdownMenu(
                hintText: 'Select CPU Socket',
                validator: (value) {
                  if (value == null) {
                    return 'Socket is required.';
                  } else {
                    return null;
                  }
                },
                itemList: _socketList,
                value: controller.spec['socket'],
                onChanged: (value) {
                  controller.spec['socket'] = value;
                },
              ),
            ),
          ),
        ],
      );
    } else if (category == 'Motherboard') {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: CustomInputField(
              label: 'Memory Type',
              child: CustomDropdownMenu(
                hintText: 'Select Memory Type',
                validator: (value) {
                  if (value == null) {
                    return 'Memory Type is required.';
                  } else {
                    return null;
                  }
                },
                itemList: _memList,
                value: controller.spec['mem_type'],
                onChanged: (value) {
                  controller.spec['mem_type'] = value;
                },
              ),
            ),
          ),
          SizedBox(width: defaultPadding),
          Expanded(
            child: CustomInputField(
              label: 'Socket',
              child: CustomDropdownMenu(
                hintText: 'Select CPU Socket',
                validator: (value) {
                  if (value == null) {
                    return 'Socket is required.';
                  } else {
                    return null;
                  }
                },
                itemList: _socketList,
                value: controller.spec['socket'],
                onChanged: (value) {
                  controller.spec['socket'] = value;
                },
              ),
            ),
          ),
        ],
      );
    } else if (category == 'RAM') {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: CustomInputField(
              label: 'Memory Type',
              child: CustomDropdownMenu(
                hintText: 'Select Memory Type',
                validator: (value) {
                  if (value == null) {
                    return 'Memory Type is required.';
                  } else {
                    return null;
                  }
                },
                itemList: _memList,
                value: controller.spec['mem_type'],
                onChanged: (value) {
                  controller.spec['mem_type'] = value;
                },
              ),
            ),
          ),
        ],
      );
    } else {
      return Container();
    }
  }
}
