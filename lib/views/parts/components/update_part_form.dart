import 'package:admin/models/part_model.dart';
import 'package:admin/views/controllers.dart';
import 'package:admin/widgets/custom_input_field.dart';
import 'package:admin/views/parts/components/spec_input_mixin.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:validators/validators.dart';
import '../../../widgets/widgets.dart';

class UpdatePartForm extends StatelessWidget with SpecInputMixin {
  UpdatePartForm({Key? key, required this.part}) : super(key: key);

  final Part part;
  final UpdatePartController controller = Get.put(UpdatePartController());

  @override
  Widget build(BuildContext context) {
    final _updateFormKey = GlobalKey<FormState>();

    return Obx(
      () => Container(
        width: MediaQuery.of(context).size.width / 3,
        child: Stack(
          alignment: Alignment.center,
          children: [
            if (controller.isLoading.value)
              Center(
                child: CircularProgressIndicator(),
              ),
            Opacity(
              opacity: controller.isLoading.value ? 0.3 : 1,
              child: Form(
                key: _updateFormKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomInputField(
                      label: 'Name',
                      child: CustomTextField(
                        controller: controller.nameController,
                        hintText: 'Name',
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a name.';
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    CustomInputField(
                      label: 'Price',
                      child: CustomTextField(
                        controller: controller.priceController,
                        hintText: 'Price',
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a price.';
                          } else if (!isNumeric(value)) {
                            return 'Please enter a numeric number.';
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    CustomInputField(
                      label: 'Description',
                      child: CustomTextField(
                        controller: controller.descController,
                        hintText: 'Description',
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a description.';
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    CustomInputField(
                      label: 'Images',
                      child: CustomImageField(
                        title: 'Change uploaded images.',
                        onPressed: controller.pickImage,
                      ),
                    ),
                    SizedBox(height: 10),
                    Obx(() {
                      return Wrap(
                        children: controller.loadedImages
                            .asMap()
                            .entries
                            .map(
                              (entry) => CustomImageThumbnail(
                                image: entry.value is XFile
                                    ? entry.value.path
                                    : entry.value,
                              ),
                            )
                            .toList(),
                      );
                    }),
                    SizedBox(height: 10),
                    buildSpecFieldInput(
                        category: part.category, controller: controller),
                    SizedBox(height: 30),
                    Row(
                      children: [
                        Expanded(
                          child: CustomPrimaryButton(
                            buttonColor: Colors.grey,
                            onPressed: () {
                              Get.close(1);
                            },
                            text: 'Cancel',
                          ),
                        ),
                        SizedBox(width: 30),
                        Expanded(
                          child: CustomPrimaryButton(
                            isLoading: controller.isLoading.value,
                            onPressed: () {
                              if (_updateFormKey.currentState!.validate()) {
                                controller.updatePart();
                              }
                            },
                            text: 'Update Part',
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
