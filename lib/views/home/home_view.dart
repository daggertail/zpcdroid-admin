import 'package:admin/responsive.dart';
import 'package:admin/views/settings/settings_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants.dart';

import 'components/header.dart';
import 'components/side_menu.dart';
import '../controllers.dart';
import '../../routing/router.dart';
import '../../routing/route_names.dart';

class HomeView extends GetWidget<NavigationController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      body: SafeArea(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // We want this side menu only for large screen
            if (Responsive.isDesktop(context))
              Expanded(
                // default flex = 1
                // and it takes 1/6 part of the screen
                child: SideMenu(),
              ),
            Expanded(
              // It takes 5/6 part of the screen
              flex: 5,
              child: Container(
                margin: EdgeInsets.all(15),
                child: Column(
                  children: [
                    Header(),
                    SizedBox(height: defaultPadding),
                    Expanded(
                      child: Navigator(
                        key: controller.navigatorKey,
                        initialRoute:
                            PartsRoute, // to change to dashboard screen later
                        onGenerateRoute: AppRouter().onGenerateRoute,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
