import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import '../../controllers.dart';
import '../../../routing/route_names.dart';

class SideMenu extends GetWidget<NavigationController> {
  const SideMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Obx(
        () => ListView(
          children: [
            DrawerHeader(
              child: Image.asset("assets/images/logo.png"),
            ),

            // DrawerListTile(
            //   title: "Dashboard",
            //   svgSrc: "assets/icons/menu_dashbord.svg",
            //   press: () {},
            // ),
            // DrawerListTile(
            //   title: "Transaction",
            //   svgSrc: "assets/icons/menu_tran.svg",
            //   press: () {},
            // ),
            // DrawerListTile(
            //   title: "Task",
            //   svgSrc: "assets/icons/menu_task.svg",
            //   press: () {},
            // ),
            // DrawerListTile(
            //   title: "Documents",
            //   svgSrc: "assets/icons/menu_doc.svg",
            //   press: () {},
            // ),
            DrawerListTile(
              title: "Parts",
              svgSrc: "assets/icons/parts.svg",
              selected: controller.selectedNav.value == 0 ? true : false,
              press: () {
                controller.navigateTo(PartsRoute, 0);
              },
            ),
            // DrawerListTile(
            //   title: "Sales",
            //   svgSrc: "assets/icons/shopping_cart.svg",
            //   selected: controller.selectedNav.value == 1 ? true : false,
            //   press: () {
            //     controller.navigateTo(SalesRoute, 1);
            //   },
            // ),
            DrawerListTile(
              title: "TS Guide",
              svgSrc: "assets/icons/guide.svg",
              selected: controller.selectedNav.value == 2 ? true : false,
              press: () {
                controller.navigateTo(TSGuideRoute, 2);
              },
            ),
            DrawerListTile(
              title: "Dictionary",
              svgSrc: "assets/icons/dictionary.svg",
              selected: controller.selectedNav.value == 3 ? true : false,
              press: () {
                controller.navigateTo(DictionaryRoute, 3);
              },
            ),
            DrawerListTile(
              title: "Settings",
              svgSrc: "assets/icons/settings.svg",
              selected: controller.selectedNav.value == 4 ? true : false,
              press: () {
                controller.navigateTo(SettingsRoute, 4);
              },
            ),
            DrawerListTile(
              title: "Logout",
              svgSrc: "assets/icons/logout.svg",
              press: () {
                final authController = Get.find<AuthController>();
                authController.signOut();
              },
            ),
          ],
        ),
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  DrawerListTile({
    Key? key,
    // For selecting those three line once press "Command+D"
    required this.title,
    required this.svgSrc,
    required this.press,
    this.selected = false,
  }) : super(key: key);

  final String title, svgSrc;
  final VoidCallback press;
  bool selected;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      selected: selected,
      selectedTileColor: Colors.white10,
      onTap: press,
      horizontalTitleGap: 0.0,
      leading: SvgPicture.asset(
        svgSrc,
        color: Colors.white54,
        height: 16,
      ),
      title: Text(
        title,
        style: TextStyle(color: Colors.white54),
      ),
    );
  }
}
