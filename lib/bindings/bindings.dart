import 'package:get/get.dart';
import '../views/controllers.dart';

class ZPCBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AuthController>(() => AuthController());
    Get.lazyPut<NavigationController>(() => NavigationController());
    Get.lazyPut<PartController>(() => PartController());
    Get.lazyPut<UpdatePartController>(() => UpdatePartController());
    Get.lazyPut<TSGuideController>(() => TSGuideController());
    Get.lazyPut<DictionaryController>(() => DictionaryController());
    Get.lazyPut<SalesController>(() => SalesController());
  }
}
