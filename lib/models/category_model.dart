class Category {
  final String name;
  final String type;

  Category({
    required this.name,
    required this.type,
  });
}
