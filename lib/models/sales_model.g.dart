// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sales_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Sales _$SalesFromJson(Map json) => Sales(
      id: json['id'] as String,
      title: json['title'] as String,
      items: (json['items'] as List<dynamic>)
          .map((e) => Part.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
      uid: json['uid'] as String,
      status: json['status'] as String,
      customerName: json['customerName'] as String?,
      createdOn:
          const TimestampConverter().fromJson(json['createdOn'] as Timestamp),
      updatedOn:
          const TimestampConverter().fromJson(json['updatedOn'] as Timestamp),
    );

Map<String, dynamic> _$SalesToJson(Sales instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'items': instance.items.map((e) => e.toJson()).toList(),
      'uid': instance.uid,
      'customerName': instance.customerName,
      'status': instance.status,
      'createdOn': const TimestampConverter().toJson(instance.createdOn),
      'updatedOn': const TimestampConverter().toJson(instance.updatedOn),
    };
