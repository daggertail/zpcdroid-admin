import 'package:cloud_firestore/cloud_firestore.dart';

class TSGuide {
  String id;
  String title;
  String desc;
  String source;
  List<dynamic> steps;

  TSGuide({
    required this.id,
    required this.title,
    required this.desc,
    required this.source,
    required this.steps,
  });

  static TSGuide fromSnapshot(DocumentSnapshot snapshot) {
    TSGuide guide = TSGuide(
      id: snapshot.id,
      desc: snapshot['desc'],
      title: snapshot['title'],
      source: snapshot['source'],
      steps: snapshot['steps'],
    );

    return guide;
  }
}
