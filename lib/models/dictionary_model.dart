import 'package:cloud_firestore/cloud_firestore.dart';

class Dictionary {
  final String id;
  final String title;
  final String desc;
  final String? citations;

  Dictionary({
    required this.id,
    required this.title,
    required this.desc,
    this.citations,
  });

  static Dictionary fromSnapshot(DocumentSnapshot snapshot) {
    Dictionary dictionary = Dictionary(
      id: snapshot.id,
      title: snapshot['title'],
      desc: snapshot['desc'],
      citations: snapshot['citations'] ?? '',
    );

    return dictionary;
  }
}
