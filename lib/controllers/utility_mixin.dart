import 'package:get/get.dart';
import 'package:flutter/material.dart';

abstract class UtilityMixin {
  void showSnackbar(String type, String title, String message) {
    Get.snackbar(
      title,
      message,
      animationDuration: const Duration(milliseconds: 500),
      duration: const Duration(milliseconds: 1500),
      backgroundColor: type == 'success'
          ? Colors.green[600]
          : type == 'error'
              ? Colors.red
              : Colors.amber,
      dismissDirection: SnackDismissDirection.HORIZONTAL,
      isDismissible: true,
    );
  }

  Future<T?> showFormDialog<T>({
    required String title,
    required Widget content,
    List<Widget>? actions,
  }) {
    return Get.defaultDialog(
      title: title,
      content: content,
      barrierDismissible: false,
      actions: actions,
      backgroundColor: Color(0xFF212332),
      radius: 0,
      titlePadding: EdgeInsets.all(10),
    );
  }

  void showModal({
    required String title,
    required List<Widget> content,
    String? confirmText,
    String? cancelText,
    VoidCallback? confirm,
    VoidCallback? cancel,
  }) {
    Get.defaultDialog(
      titlePadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      contentPadding: const EdgeInsets.all(20),
      title: title,
      titleStyle: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      content: Column(
        children: content.toList(),
      ),
      radius: 0,
      confirm: MaterialButton(
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
        elevation: 0,
        color: Colors.blue,
        onPressed: confirm,
        child: Text(
          confirmText ?? 'Save',
          style: const TextStyle(color: Colors.white),
        ),
      ),
      cancel: MaterialButton(
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
        color: Colors.red,
        onPressed: cancel ??
            () {
              Get.back();
            },
        child: Text(cancelText ?? 'Cancel'),
      ),
    );
  }
}
