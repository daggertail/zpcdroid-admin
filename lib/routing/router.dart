import 'package:admin/views/dictionary/dictionary_view.dart';
import 'package:admin/views/tsguide/tsguide_view.dart';
import 'package:flutter/material.dart';
import '../views/views.dart';
import 'route_names.dart';

class AppRouter {
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    print('generateRoute: ${settings.name}');
    switch (settings.name) {
      case LoginRoute:
        return _getPageRoute(LoginView());

      case HomeRoute:
        return _getPageRoute(HomeView());

      case SalesRoute:
        return _getPageRoute(SalesView());

      case PartsRoute:
        return _getPageRoute(PartsView());

      case TSGuideRoute:
        return _getPageRoute(TSGuideView());

      case DictionaryRoute:
        return _getPageRoute(DictionaryView());

      case SettingsRoute:
        return _getPageRoute(SettingsView());

      default:
        return _errorRoute();
    }
  }

  Route _errorRoute() {
    return MaterialPageRoute(
      settings: RouteSettings(name: 'error'),
      builder: (BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('404: Cannot find page'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('We cannot find the page.',
                  style: Theme.of(context).textTheme.headline2),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  Navigator.pushReplacementNamed(context, '/');
                },
                child: Text('Go back to home page'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  PageRoute _getPageRoute(Widget child) {
    return MaterialPageRoute(builder: (context) => child);
  }
}
