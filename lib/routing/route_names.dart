const String LoginRoute = 'login';
const String HomeRoute = 'home';
const String PartsRoute = 'parts';
const String SalesRoute = 'sales';
const String TSGuideRoute = 'tsguide';
const String DictionaryRoute = 'dictionary';
const String SettingsRoute = 'settings';
