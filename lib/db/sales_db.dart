import 'package:admin/models/sales_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

const kCollectionName = 'sales';

class SalesDB {
  final FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;

  Stream<List<Sales>> getAllSales() {
    return _firebaseFirestore
        .collection(kCollectionName)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return Sales.fromJson({'id': doc.id, ...doc.data()});
      }).toList();
    });
  }

  Future<void> delete(String docId) async {
    CollectionReference _fb = _firebaseFirestore.collection(kCollectionName);

    try {
      await _fb.doc(docId).update({'deleted': true});
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateSaleStatus(String id, String newStatus) async {
    CollectionReference _fb = _firebaseFirestore.collection(kCollectionName);

    try {
      await _fb.doc(id).update({'status': newStatus});
    } catch (e) {
      print(e);
    }
  }
}
