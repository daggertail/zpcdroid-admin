import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/dictionary_model.dart';

const kCollectionName = 'dictionary';

class DictionaryDB {
  final FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;

  Stream<List<Dictionary>> getAll() {
    return _firebaseFirestore
        .collection(kCollectionName)
        .orderBy('title')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return Dictionary.fromSnapshot(doc);
      }).toList();
    });
  }

  Future<void> createTerm(Map<String, dynamic> data) async {
    CollectionReference _fb = _firebaseFirestore.collection(kCollectionName);
    String id = _fb.doc().id;

    try {
      await _fb.doc(id).set({'id': id, ...data});
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateTerm(Map<String, dynamic> data) async {
    CollectionReference _fb = _firebaseFirestore.collection(kCollectionName);

    String docId = data['id'];
    final Map<String, dynamic> newData = {
      'title': data['title'],
      'desc': data['desc'],
      'citations': data['citations']
    };

    try {
      await _fb.doc(docId).update(newData);
    } catch (e) {
      print(e);
    }
  }

  Future<void> deleteTerm(String docId) async {
    CollectionReference _fb = _firebaseFirestore.collection(kCollectionName);

    try {
      await _fb.doc(docId).delete();
    } catch (e) {
      print(e);
    }
  }
}
