import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import '../models/part_model.dart';

import 'package:path/path.dart' as p;

const kCollectionName = 'parts2';

class PartDB {
  final FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;

  Stream<List<Part>> getAllParts() {
    return _firebaseFirestore
        .collection(kCollectionName)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return Part.fromJson({'id': doc.id, ...doc.data()});
      }).toList();
    });
  }

  Query<Map<String, dynamic>> getPaginatedParts() {
    return _firebaseFirestore.collection(kCollectionName).orderBy('name');
  }

  Stream<List<Part>> getAllPartsByCategory(String category) {
    return _firebaseFirestore
        .collection(kCollectionName)
        .where('category', isEqualTo: category)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return Part.fromJson({'id': doc.id, ...doc.data()});
      }).toList();
    });
  }

  Future<List<dynamic>> uploadImages(String id, List<dynamic> images) async {
    List<dynamic> imagesLink = [];

    TaskSnapshot task;
    String ref;

    try {
      for (var i = 0; i < images.length; i++) {
        ref = "images/$id/${images[i].name}";
        task = await FirebaseStorage.instance.ref().child(ref).putData(
              await images[i].readAsBytes(),
              SettableMetadata(contentType: images[i].mimeType),
            );

        // task.snapshotEvents.listen((event) async {
        //   imagesLink.add(await event.ref.getDownloadURL());
        // });

        if (task.state == TaskState.success) {
          imagesLink.add(await task.ref.getDownloadURL());
        } else if (task.state == TaskState.error) {
          throw ('Error uploading images');
        }
      }
    } catch (e) {
      print(e);
    }

    return imagesLink;
  }

  Future<void> updatePart(
      Map<String, dynamic> data, List<dynamic> images) async {
    CollectionReference _fb = _firebaseFirestore.collection(kCollectionName);

    String docId = data['id'];

    final Map<String, dynamic> newData = {
      'name': data['name'],
      'category': data['category'],
      'price': data['price'],
      'desc': data['desc'],
      'spec': data['spec'],
      'competitorsPrice': data['competitorsPrice'],
      'updatedOn': DateTime.now()
    };

    if (images.isNotEmpty) {
      // Check if the images needs updating
      if (images[0] is XFile) {
        newData['images'] = await uploadImages(docId, images);
      }
    }

    try {
      await _fb.doc(docId).update(newData);
    } catch (e) {
      print(e);
    }
  }

  Future<void> createPart(
      Map<String, dynamic> data, List<dynamic> images) async {
    CollectionReference _fb = _firebaseFirestore.collection(kCollectionName);
    String id = _fb.doc().id;

    final Map<String, dynamic> newData = {
      'name': data['name'],
      'category': data['category'],
      'price': data['price'],
      'desc': data['desc'],
      'spec': data['spec'],
      'competitorsPrice': data['competitorsPrice'],
      'updatedOn': DateTime.now()
    };

    if (images.isNotEmpty) {
      newData['images'] = await uploadImages(id, images);
    }

    try {
      await _fb.doc(id).set(newData);
    } catch (e) {
      print(e);
    }
  }

  Future<void> delete(String docId) async {
    CollectionReference _fb = _firebaseFirestore.collection(kCollectionName);
    try {
      await _fb.doc(docId).delete();
    } catch (e) {
      print(e);
    }
  }
}
