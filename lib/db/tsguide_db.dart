import 'package:admin/models/tsguide_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

const kCollectionName = 'tsguides';

class TSGuideDB {
  final FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;

  Stream<List<TSGuide>> getAll() {
    return _firebaseFirestore
        .collection(kCollectionName)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return TSGuide.fromSnapshot(doc);
      }).toList();
    });
  }

  Future<void> delete(String docId) async {
    CollectionReference _fb = _firebaseFirestore.collection(kCollectionName);

    try {
      await _fb.doc(docId).delete();
    } catch (e) {
      print(e);
    }
  }

  Future<void> createGuide(Map<String, dynamic> data) async {
    CollectionReference _fb = _firebaseFirestore.collection(kCollectionName);
    String id = _fb.doc().id;

    try {
      await _fb.doc(id).set({'id': id, ...data});
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateGuide(Map<String, dynamic> data) async {
    CollectionReference _fb = _firebaseFirestore.collection(kCollectionName);

    String docId = data['id'];
    final Map<String, dynamic> newData = {
      'title': data['title'],
      'source': data['source'] ?? '',
      'desc': data['desc'] ?? '',
      'steps': data['steps']
    };

    try {
      await _fb.doc(docId).update(newData);
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateDB() async {
    try {
      var querySnapshots =
          await _firebaseFirestore.collection(kCollectionName).get();

      for (var doc in querySnapshots.docs) {
        await doc.reference.update({
          'source': '',
        });
      }
    } catch (e) {
      print(e);
    }
  }
}
